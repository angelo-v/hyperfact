# Hyperfact

Store data as a unified JSON-LD graph.

This experimental. Expect things to change.

# Quick start

```ecmascript 6
import createStore from "hyperfact";

const facts = createStore();

// add facts to the store (this is async!)
facts.merge({
              "@context": "http://schema.org/",
              "@id": "https://people.example/profile/jane#me",
              "@type": "Person",
              "name": "Jane Doe",
              "jobTitle": "Professor",
            });
            
// subscribe to changes
facts.subscribe('https://people.example/profile/jane#me', (current, prev) => {
  console.log('updated', {current, prev});
});

// get facts
const data = facts.getResource('https://people.example/profile/jane#me');
console.log()
```
