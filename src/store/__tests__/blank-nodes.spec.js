import createStore from '../store';

describe('merging blank nodes', function () {
  describe('GIVEN a store with a blank node', () => {

    let facts;
    beforeEach(async () => {
      facts = createStore();
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        "name": "Jane Doe",
        "address": {
          "addressLocality": "Capital City"
        }
      })
    });

    describe('WHEN new data with a different blank node is merged', () => {

      beforeEach(async () => {
        await facts.merge({
          "@context": {
            "@vocab": "http://schema.org/"
          },
          "@type": "Person",
          "@id": "/person/43",
          "name": "John Doe",
          "address": {
            "addressLocality": "Different City"
          }
        })
      });

      it('THEN both nodes get different blank node identifiers', () => {
        expect(facts.all()).toEqual(
            {
              "@graph": [
                {
                  "@id": "/person/42",
                  "@type": "http://schema.org/Person",
                  "http://schema.org/address": {"@id": "_:b0"},
                  "http://schema.org/name": "Jane Doe"
                }, {
                  "@id": "/person/43",
                  "@type": "http://schema.org/Person",
                  "http://schema.org/address": {"@id": "_:b1"},
                  "http://schema.org/name": "John Doe"
                }, {
                  "@id": "_:b0",
                  "http://schema.org/addressLocality": "Capital City"
                }, {
                  "@id": "_:b1",
                  "http://schema.org/addressLocality": "Different City"
                }
              ]
            }
        )
      });

    });

    describe('WHEN new data with "the same" blank node is merged', () => {

      beforeEach(async () => {
        await facts.merge({
          "@context": {
            "@vocab": "http://schema.org/"
          },
          "@type": "Person",
          "@id": "/person/42",
          "address": {
            "streetAddress": 'Main street'
          }
        })
      });

      it('THEN it gets a different blank node identifier, because it cannot be identified as the existing one',
          () => {
            expect(facts.all()).toEqual(
                {
                  "@graph": [
                    {
                      "@id": "/person/42",
                      "@type": "http://schema.org/Person",
                      "http://schema.org/address": [{
                        "@id": "_:b0"
                      }, {
                        "@id": "_:b1"
                      }],
                      "http://schema.org/name": "Jane Doe"
                    }, {
                      "@id": "_:b0",
                      "http://schema.org/addressLocality": "Capital City"
                    }, {
                      "@id": "_:b1",
                      "http://schema.org/streetAddress": "Main street"
                    }
                  ]
                }
            )
          });

    });

  });

});