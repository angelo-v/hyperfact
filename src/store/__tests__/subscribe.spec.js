import createStore from '../store';


describe('subscriptions', () => {

  let facts;

  describe('GIVEN a store including /person/42 and subscribers for /person/42 and /person/43', function () {

    let handler1, handler2, handler3, otherHandler;
    beforeEach(async () => {
      // given: a store
      facts = createStore();

      // and: a subscription for /person/42
      handler1 = jest.fn();
      handler2 = jest.fn();
      handler3 = jest.fn();
      otherHandler = jest.fn();
      facts.subscribe('/person/42', handler1);
      facts.subscribe('/person/42', handler2);
      facts.subscribe('/person/42', handler3);
      facts.subscribe('/person/43', otherHandler);
      expect(handler1).not.toHaveBeenCalled();
      expect(handler2).not.toHaveBeenCalled();
      expect(handler3).not.toHaveBeenCalled();
      expect(otherHandler).not.toHaveBeenCalled();

      // and: facts about /person/42
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        name: "Jane Doe"
      });
    });

    describe('WHEN new data about /person/42 is merged', function () {

      beforeEach(async () => {
        // when: new data about /person/42 is added
        await facts.merge({
          "@context": {
            "@vocab": "http://schema.org/"
          },
          "@id": "/person/42",
          givenName: "Jane"
        });
      });

      it('THEN all subscribers of /person/42 should be notified about the previous and current data of /person/42', async (done) => {
        // then: the subscriber gets notified
        setTimeout(() => {
          expect(handler1).toHaveBeenCalledWith({
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/givenName": "Jane",
            "http://schema.org/name": "Jane Doe"
          }, {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe"
          });
          expect(handler2).toHaveBeenCalledWith({
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/givenName": "Jane",
            "http://schema.org/name": "Jane Doe"
          }, {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe"
          });
          expect(handler3).toHaveBeenCalledWith({
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/givenName": "Jane",
            "http://schema.org/name": "Jane Doe"
          }, {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe"
          });
          done()
        });
      });

      it('BUT subscribers of /person/43 should not be notfied', async (done) => {
        setTimeout(() => {
          expect(otherHandler).not.toHaveBeenCalled();
          done();
        });
      });

    });

    it('EXPECT subscribers not to be notified after unsubscribing', async (done) => {
      // when: the subscription for /person/42 is cancled
      facts.unsubscribe('/person/42', handler2);
      // when: new data about /person/42 is added
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@id": "/person/42",
        givenName: "John"
      });

      setTimeout(() => {
        // then: the cancled subscription for /person/42 does not get notified
        expect(handler2).not.toHaveBeenCalled();
        // but: the others still are
        expect(handler1).toHaveBeenCalled();
        expect(handler3).toHaveBeenCalled();
        done();
      });
    });



  });

  describe('GIVEN a store including /person/42, her address and subscriptions for both', function () {
    let onPersonUpdate, onAddressUpdate;
    beforeEach(async () => {
      // given: a store with facts about /person/42 and /address/1337
      facts = createStore();

      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        name: "Jane Doe",
        address: {
          "@id": "/address/1337",
          addressLocality: 'Capital City'
        }
      });
      // and: a subscription for /person/42
      onPersonUpdate = jest.fn();
      onAddressUpdate = jest.fn();
      facts.subscribe('/person/42', onPersonUpdate);
      facts.subscribe('/address/1337', onAddressUpdate);
    });

    describe('WHEN both, person and address, are updated', function () {
      beforeEach(async () => {
        // when: new data about /person/42 is added
        await facts.merge({
          "@context": {
            "@vocab": "http://schema.org/"
          },
          "@id": "/person/42",
          givenName: "Jane",
          address: {
            '@id': "/address/1337",
            addressLocality: 'Capital City',
            streetAddress: 'Main street'
          }
        });
      });

      it('THEN the person subscription should be notified', (done) => {
        setTimeout(() => {
          expect(onPersonUpdate).toHaveBeenCalledWith({
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/givenName": "Jane",
            "http://schema.org/name": "Jane Doe",
            "http://schema.org/address": {
              '@id': "/address/1337",
              "http://schema.org/addressLocality": "Capital City",
              "http://schema.org/streetAddress": "Main street",
            }
          }, {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe",
            "http://schema.org/address": {
              '@id': "/address/1337",
              "http://schema.org/addressLocality": "Capital City",
            }
          });
          done()
        });
      });

      it('AND the address subscription should be notified', (done) => {
        setTimeout(() => {
          expect(onAddressUpdate).toHaveBeenCalledWith({
            "@id": "/address/1337",
            "http://schema.org/addressLocality": "Capital City",
            "http://schema.org/streetAddress": "Main street",
          }, {
            "@id": "/address/1337",
            "http://schema.org/addressLocality": "Capital City",
          });
          done()
        });
      });

    });

  })

  describe(
      'GIVEN a store with custom context including /person/42 and subscribers for that resource',
      function () {

        let handler;
        beforeEach(async () => {
          // given: a store with facts about /person/42
          facts = createStore({
            "@vocab": "http://schema.org/",
            "@base": "http://api.example/"
          });

          await facts.merge({
            "@context": {
              "@vocab": "http://schema.org/",
              "@base": "http://api.example/"
            },
            "@type": "Person",
            "@id": "person/42",
            "name": "Jane Doe"
          });
          // and: a subscription for /person/42
          handler = jest.fn();
          facts.subscribe('person/42', handler);
          expect(handler).not.toHaveBeenCalled();
        });

        describe('WHEN new data about person/42 is merged', function () {

          beforeEach(async () => {
            // when: new data about /person/42 is added
            await facts.merge({
              "@context": {
                "@vocab": "http://schema.org/",
                "@base": "http://api.example/"
              },
              "@id": "person/42",
              "givenName": "Jane"
            });
          });

          it('THEN the subscriber is notified about the previous and current data of /person/42 using the store context',
              async (done) => {
                // then: the subscriber gets notified
                setTimeout(() => {
                  expect(handler).toHaveBeenCalledWith({
                    "@context": {
                      "@vocab": "http://schema.org/",
                      "@base": "http://api.example/"
                    },
                    "@id": "person/42",
                    "@type": "Person",
                    "givenName": "Jane",
                    "name": "Jane Doe"
                  }, {
                    "@context": {
                      "@vocab": "http://schema.org/",
                      "@base": "http://api.example/"
                    },
                    "@id": "person/42",
                    "@type": "Person",
                    "name": "Jane Doe"
                  });
                  done()
                });
              });

        });
      });

});