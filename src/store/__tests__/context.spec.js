import createStore from '../store';

describe('store with custom context', () => {

  const customContext = {
    "@vocab": "http://schema.org/",
    "@base": "https://base.example/"
  };

  let facts;

  beforeEach(async () => {

    facts = createStore(customContext);
    await facts.merge({
      "@context": customContext,
      "@id": "person/42",
      "@type": "Person",
      name: "Jane Doe",
      address: {
        "@id": "address/1337",
        addressLocality: 'Capital City'
      }
    })
  });

  it('should get data with context applied', () => {
    expect(facts.all()).toEqual({
      "@context": customContext,
      "@graph": [{
        "@id": "address/1337",
        addressLocality: 'Capital City'
      }, {
        "@id": "person/42",
        "@type": "Person",
        name: "Jane Doe",
        address: {
          "@id": "address/1337"
        }
      }]
    });
  });

  it('should apply context to data sent to subscribers', async (done) => {
    const onPersonUpdate = jest.fn();
    const onAddressUpdate = jest.fn();
    facts.subscribe('person/42', onPersonUpdate);
    facts.subscribe('address/1337', onAddressUpdate);
    facts.merge({
      "@context": customContext,
      "@id": "person/42",
      givenName: "Jane",
      address: {
        "@id": "address/1337",
        streetAddress: 'Main street'
      }
    });
    setTimeout(() => {
      expect(onPersonUpdate).toHaveBeenCalledWith({
        "@context": customContext,
        "@id": "person/42",
        "@type": "Person",
        "givenName": "Jane",
        "name": "Jane Doe",
        "address": {
          '@id': "address/1337",
          "addressLocality": "Capital City",
          "streetAddress": 'Main street'
        }
      }, {
        "@context": customContext,
        "@id": "person/42",
        "@type": "Person",
        "name": "Jane Doe",
        "address": {
          '@id': "address/1337",
          "addressLocality": "Capital City"
        }
      });
      expect(onAddressUpdate).toHaveBeenCalledWith({
        "@context": customContext,
        "@id": "address/1337",
        "addressLocality": "Capital City",
        "streetAddress": 'Main street'
      }, {
        "@context": customContext,
        "@id": "address/1337",
        "addressLocality": "Capital City",
      });
      done()
    });
  });

});
