import createStore from '../store';

const facts = createStore();

describe('immutable store data', () => {
  it('cannot be mutated manually', async () => {

    // given: a store with facts
    const newFacts = await facts.merge({
      "@context": {
        "@vocab": "http://schema.org/"
      },
      "@type": "Person",
      "@id": "/person/42",
      name: "Jane Doe"
    });
    const person = newFacts.all()['@graph'][0];

    // when: I try to change the data by hand
    expect(() => {
      person["http://schema.org/name"] = 'changed';
    }).toThrow(
        new TypeError(
            "Cannot assign to read only property 'http://schema.org/name' of object '#<Object>'"
        )
    );

    // then: It does not affect the actual data
    expect(newFacts.all()).toEqual({
      "@graph": [
        {
          "@id": "/person/42",
          "@type": "http://schema.org/Person",
          "http://schema.org/name": "Jane Doe"
        }
      ]
    });
  });
});