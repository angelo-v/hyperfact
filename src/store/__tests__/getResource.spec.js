import createStore from '../store';

describe('get data for specific resource', () => {

  describe('GIVEN two persons in a store without context', function () {
    let facts;
    beforeEach(async () => {
      facts = createStore();
      await facts.merge({
        "@context": {
          "@vocab": "https://schema.org/",
          "@base": "https://api.example"
        },
        "@graph": [
          {
            "@id": "person/1",
            "@type": "Person",
            "name": "Jane Doe"
          },
          {
            "@id": "person/2",
            "@type": "Person",
            "name": "John Doe"
          }
        ]
      })
    });

    describe('WHEN I retrieve person/1 from the store', () => {

      let result;
      beforeEach(async () => {
        result = await facts.getResource('https://api.example/person/1')
      });

      it('THEN I get all data of person/1 without context', () => {
        expect(result).toEqual({
          "@id": "https://api.example/person/1",
          "@type": "https://schema.org/Person",
          "https://schema.org/name": "Jane Doe"
        });
      });

    });

    describe('WHEN I retrieve person/1 from the store using a specific context', () => {

      let result;
      const requestedContext = {
        "@base": "https://api.example/person/",
        "Person": "https://schema.org/Person",
        "full_name": "https://schema.org/name"
      };

      beforeEach(async () => {
        result = await facts.getResource('1', requestedContext)
      });

      it('THEN I get all data of person/1 respecting that context', () => {
        expect(result).toEqual({
          "@context": requestedContext,
          "@id": "1",
          "@type": "Person",
          "full_name": "Jane Doe"
        });
      });

    });

  });

  describe('GIVEN two persons in a store with a custom context', function () {
    let facts;
    beforeEach(async () => {
      facts = createStore({
        "@vocab": "https://schema.org/",
        "@base": "https://api.example/"
      });
      await facts.merge({
        "@context": {
          "@vocab": "https://schema.org/",
          "@base": "https://api.example"
        },
        "@graph": [
          {
            "@id": "person/1",
            "@type": "Person",
            "name": "Jane Doe"
          },
          {
            "@id": "person/2",
            "@type": "Person",
            "name": "John Doe"
          }
        ]
      })
    });

    describe('WHEN I retrieve person/1 from the store', () => {

      let result;
      beforeEach(async () => {
        result = await facts.getResource('person/1')
      });

      it('THEN I get all data of person/1 with the store context', () => {
        expect(result).toEqual({
          "@context": {
            "@vocab": "https://schema.org/",
            "@base": "https://api.example/"
          },
          "@id": "person/1",
          "@type": "Person",
          "name": "Jane Doe"
        });
      });

    });

    describe('WHEN I retrieve person/1 from the store using a specific context',
        () => {

          let result;
          const requestedContext = {
            "@base": "https://api.example/person/",
            "Person": "https://schema.org/Person",
            "full_name": "https://schema.org/name"
          };

          beforeEach(async () => {
            result = await facts.getResource('1', requestedContext)
          });

          it('THEN I get all data of person/1 respecting that context', () => {
            expect(result).toEqual({
              "@context": requestedContext,
              "@id": "1",
              "@type": "Person",
              "full_name": "Jane Doe"
            });
          });

        });

  });
});
